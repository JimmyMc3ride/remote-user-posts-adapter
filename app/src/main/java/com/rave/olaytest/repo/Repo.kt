package com.rave.olaytest.repo

import com.rave.olaytest.models.ApiResponseState
import com.rave.olaytest.remote.UserPostService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

object Repo {
    private val api by lazy { UserPostService.api }

    suspend fun getUserPosts() = withContext(Dispatchers.IO) {
        try {
            val res = api.getPosts()
            if (res.isSuccessful && res.body() != null) {
                return@withContext ApiResponseState.Success(data = res.body()!!)
            } else {
                return@withContext ApiResponseState.Error(message = "Oops, something went wrong...")
            }
        } catch (e: Exception) {
            return@withContext ApiResponseState.Error(e.message.toString())
        }
    }
}