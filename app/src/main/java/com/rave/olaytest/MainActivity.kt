package com.rave.olaytest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.rave.olaytest.adapters.UserPostAdapter
import com.rave.olaytest.databinding.ActivityMainBinding
import com.rave.olaytest.models.ApiResponseState
import com.rave.olaytest.utils.collectAwareFlow

class MainActivity : AppCompatActivity() {
    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    val viewModel by lazy { ViewModelProvider(this)[MainActivityViewModel::class.java] }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initCollectors()
    }

    private fun initCollectors() = with(binding) {
        collectAwareFlow(viewModel.posts) { postResponseState ->
            when (postResponseState) {
                is ApiResponseState.Success -> {
                    rvUserPosts.adapter = UserPostAdapter(postResponseState.data)
                }
                is ApiResponseState.Error -> {
                    Log.d(TAG, "Error: ${postResponseState.message}")
                }
                ApiResponseState.Idle -> {
                    Log.d(TAG, "Idle")
                }
                ApiResponseState.Loading -> {
                    Log.d(TAG, "Loading...")
                }
            }
        }
    }

    companion object {
        const val TAG = "MainActivity"
    }
}