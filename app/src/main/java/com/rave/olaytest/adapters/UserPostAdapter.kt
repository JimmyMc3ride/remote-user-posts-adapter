package com.rave.olaytest.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.olaytest.databinding.ItemUserPostBinding
import com.rave.olaytest.models.UserPost

class UserPostAdapter(
    private val posts: List<UserPost>
) : RecyclerView.Adapter<UserPostAdapter.UserPostViewHolder>() {

    class UserPostViewHolder(private val binding: ItemUserPostBinding) :
        RecyclerView.ViewHolder(binding.root) {

            fun loadUserPost(userPost: UserPost) = with(binding) {
                tvTitle.text = userPost.title
                tvBody.text = userPost.body
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ItemUserPostBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ).let { binding ->
        UserPostViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserPostViewHolder, position: Int) {
        holder.loadUserPost(posts[position])
    }

    override fun getItemCount() = posts.size
}