package com.rave.olaytest

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.olaytest.models.ApiResponseState
import com.rave.olaytest.models.UserPost
import com.rave.olaytest.repo.Repo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainActivityViewModel : ViewModel() {
    private val repo = Repo

    private val _posts = MutableStateFlow<ApiResponseState<List<UserPost>>>(ApiResponseState.Idle)
    val posts = _posts.asStateFlow()

    init {
        _posts.value = ApiResponseState.Loading
        viewModelScope.launch(Dispatchers.Main) {
            _posts.value = repo.getUserPosts()
        }
    }
}