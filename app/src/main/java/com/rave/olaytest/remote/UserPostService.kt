package com.rave.olaytest.remote

import com.rave.olaytest.models.UserPost
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface UserPostService {
    @GET("/posts")
    suspend fun getPosts(): Response<List<UserPost>>

    companion object {
        val api = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://jsonplaceholder.typicode.com")
            .build()
            .create(UserPostService::class.java)
    }
}