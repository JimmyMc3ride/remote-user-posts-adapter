package com.rave.olaytest.models

sealed class ApiResponseState<out T> {
    object Idle: ApiResponseState<Nothing>()
    object Loading: ApiResponseState<Nothing>()
    data class Success<T>(val data: T): ApiResponseState<T>()
    data class Error(val message: String): ApiResponseState<Nothing>()
}
