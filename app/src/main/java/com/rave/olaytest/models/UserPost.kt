package com.rave.olaytest.models

data class UserPost(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String,
)
